a, b = sorted([int(x) for x in input().split()])

if b % a == 0:
    print('Sao Multiplos')
else:
    print('Nao sao Multiplos')