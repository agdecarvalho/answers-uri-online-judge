def even(num):
    return sum([1 for i in num if i % 2 == 0])

print('%d valores pares' % even(float(input()) for x in range(5)))
