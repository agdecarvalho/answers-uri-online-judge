from unittest import TestCase
from Q1051 import salary


class SalaryTestCase(TestCase):

    def test_salary_when_2000_then_Isento(self):
        a = salary(2000)
        self.assertEquals(a, 'Isento')

    def test_salary_when_2500_then_40(self):
        a = salary(2500)
        self.assertEquals(a, 'R$ %.2f' % 40)
