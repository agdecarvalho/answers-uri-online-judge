a, b, c = [float(x) for x in input().split()]
delt = b**2-4*a*c

if a != 0 and delt >= 0:
    R1 = (-b+delt**0.5)/(2*a)
    R2 = (-b-delt**0.5)/(2*a)
    print('R1 = %.5f' % R1)
    print('R2 = %.5f' % R2)
else:
    print('Impossivel calcular')