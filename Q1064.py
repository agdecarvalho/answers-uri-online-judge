num = [float(input()) for x in range(6)]

def sum_positives(num):
    m = [1 for i in num if i > 0]
    return sum(m)

def middle_positives(num):
    n = [float(i) for i in num if i > 0]
    return sum(n)/len(n)

print('%d valores positivos' % sum_positives(num))
print('%.1f' % middle_positives(num))
