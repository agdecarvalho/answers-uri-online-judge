from unittest import TestCase
from Q1060 import sum_positives


class PositiveTestCase(TestCase):

    def test_positive_when_all_neg_then_0(self):
        lst = [-7.5, -5.2, -4, -0.3, -9.88, -7]
        pos = sum_positives(lst)
        self.assertEquals(pos, 0)

    def test_positive_when_all_pos_then_6(self):
        lst = [7.5, 5.2, 4, 0.3, 9.88, 7]
        pos = sum_positives(lst)
        self.assertEquals(pos, 6)

    def test_positive_when_three_pos_then_3(self):
        lst = [-7.5, -5.2, -4, 0.3, 9.88, 7]
        pos = sum_positives(lst)
        self.assertEquals(pos, 3)
