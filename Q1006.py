grades = []
weights = [2, 3, 5]

for weight in weights:
    grades.append(float(input())*weight)

print('MEDIA = %.1f' % (sum(grades)/sum(weights)))

# list comprehension:
# result = sum([float(input())*weight for weight in [2, 3, 5]])/sum([2, 3, 5])
# print('MEDIA = %.1f' % (result))