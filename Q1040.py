weights = [2, 3, 4, 1]
grades = [float(x) for x in input().split()]
soma = [grades[i]*weights[i] for i in range(4)]
average = sum(soma)/sum(weights)

print('Media: %.1f' % (average))

if average >= 7.0:
    print('Aluno aprovado.')
elif 5.0 <= average < 7.0:
    print('Aluno em exame.')
    exame = float(input())
    print('Nota do exame: %.1f' % (exame))
    final_average = (average+exame)/2
    if final_average >= 5:
        print('Aluno aprovado.')
    else:
        print('Aluno reprovado.')
    print('Media final: %.1f' % (final_average))
else:
    print('Aluno reprovado.')