seconds = int(input())

h = seconds//365
m = (seconds-h*365)//30
s = seconds-h*365-m*30

print('%d ano(s)\n%d mes(es)\n%d dia(s)' % (h, m, s))
