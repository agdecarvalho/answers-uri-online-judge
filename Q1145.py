x, y = [int(c) for c in input().split()]

for i in range(1,y+1):
    if i % x == 0:
        print(i)
    else:
        print('%d ' % i,end='')
