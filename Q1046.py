a, b = [int(x) for x in input().split()]

if a > b:
    print('O JOGO DUROU %d HORA(S)' % ((b+24)-a))
if a == b:
    print('O JOGO DUROU 24 HORA(S)')
if a < b:
    print('O JOGO DUROU %d HORA(S)' % (b-a))