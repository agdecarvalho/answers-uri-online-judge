def six_odd(n,m):
    if n == m:
        print(0)
    else:
        if n % 2 == 0:
            print(sum(range(n+1,m,2)))
        else:
            print(sum(range(n+2,m,2)))

lst = sorted([int(input()) for i in range(2)])
n, m = lst
six_odd(n,m)
