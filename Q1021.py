valor = float(input())
tipo = 'nota'
divisores = [100, 50, 20, 10, 5, 2, 1, 0.5, 0.25, 0.10, 0.05, 0.01]

print('NOTAS:')

for indice, divisor in enumerate(divisores):
    resultado = valor
        
    if indice == 6:
        tipo = 'moeda'
        print('MOEDAS:')
        
    for posicao in range(indice+1):
        if posicao == indice:
            resultado = resultado // divisores[posicao]
            print('%d %s(s) de R$ %.2f' %(resultado, tipo, divisor))
        else:
            resultado = resultado % divisores[posicao]
