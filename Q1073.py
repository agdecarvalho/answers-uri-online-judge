# Using a generator (yield)

def lista(num):
    for i in range(1,num+1):
        if i % 2 == 0:
            yield i

def even_square(lista):
    for n in lista:
        print('%d^2 = %d' % (n, n**2))

num = int(input())
even_square(lista(num))
