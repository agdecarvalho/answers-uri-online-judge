amount = int(input())

hundred = amount//100
fifty = (amount%100)//50
twenty = ((amount%100)%50)//20
ten = (((amount%100)%50)%20)//10 
five = ((((amount%100)%50)%20)%10)//5
two = (((((amount%100)%50)%20)%10)%5)//2
one = ((((((amount%100)%50)%20)%10)%5)%2)//1

print(amount)
print('%d nota(s) de R$ 100,00' % hundred)
print('%d nota(s) de R$ 50,00' % fifty)
print('%d nota(s) de R$ 20,00' % twenty)
print('%d nota(s) de R$ 10,00' % ten)
print('%d nota(s) de R$ 5,00' % five)
print('%d nota(s) de R$ 2,00' % two)
print('%d nota(s) de R$ 1,00' % one)