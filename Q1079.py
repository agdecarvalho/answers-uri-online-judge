num = int(input())
lista = [input().split() for n in range(num)]

for a, b, c in lista:
    average = ((float(a)*2 + float(b)*3 + float(c)*5)/(2+3+5))
    print('%.1f' % average)
