num = int(input())
lista = [int(input()) for n in range(num)]

def even_odd(lista):
    for n in lista:
        if n == 0:
            print('NULL')
        elif n > 0:
            if n % 2 == 0:
                print('EVEN POSITIVE')
            else:
                print('ODD POSITIVE')
        else:
            if n % 2 == 0:
                print('EVEN NEGATIVE')
            else:
                print('ODD NEGATIVE')

even_odd(lista)
