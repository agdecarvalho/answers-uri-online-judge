num = [float(input()) for x in range(5)]

def even(num):
    k = [1 for i in num if i % 2 == 0]
    return sum(k)

def odd(num):
    l = [1 for i in num if i % 2 != 0]
    return sum(l)

def sum_positives(num):
    m = [1 for i in num if i > 0]
    return sum(m)

def sum_negatives(num):
    n = [1 for i in num if i < 0]
    return sum(n)

print('%d valor(es) par(es)' % even(num))
print('%d valor(es) impar(es)' % odd(num))
print('%d valor(es) positivo(s)' % sum_positives(num))
print('%d valor(es) negativo(s)' % sum_negatives(num))
