num = int(input())
lista = [int(input()) for n in range(num)]

def in_interval(lista):
    a = [i for i in lista if 10 <= i <= 20]
    return len(a)

def out_interval(lista):
    b = [i for i in lista if i < 10 or i > 20]
    return len(b)

print('%d in' % in_interval(lista))
print('%d out' % out_interval(lista))
