a, b, c = sorted([float(x) for x in input().split()])

if c < (a + b):
    print('Perimetro = %.1f' % (a+b+c))
else:
    area = ((c+b)*a)/2
    print('Area = %.1f' % (area))