# Using try and except

num = int(input())
lista = [input().split() for n in range(num)]

for x, y in lista:
    try:
        div = float(x)/float(y)
        print('%.1f' % div)
    except ZeroDivisionError:
        print('divisao impossivel')
