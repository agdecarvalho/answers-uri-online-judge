from unittest import TestCase
from Q1052 import month


class MonthTestCase(TestCase):

    def test_month_when_4_then_april(self):
        mes = month(4)
        self.assertEquals(mes, 'April')

    def test_month_when_12_then_december(self):
        mes = month(12)
        self.assertEquals(mes, 'December')
