def sum_positives(num):
    return sum([1 for i in num if i > 0])

# print('%d valores positivos' % sum_positives(float(input()) for x in range(6)))
