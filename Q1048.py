x = float(input())

if 0 <= x <= 400:
    a = x + x * 0.15
    b = x * 0.15
    c = 15

    print('Novo salario: %.2f' % (a))
    print('Reajuste ganho: %.2f' % (b))
    print('Em percentual: %d %%' % (c))

elif 400.01 <= x <= 800.00:
    a = x + x * 0.12
    b = x * 0.12
    c = 12

    print('Novo salario: %.2f' % (a))
    print('Reajuste ganho: %.2f' % (b))
    print('Em percentual: %d %%' % (c))

elif 800.01 <= x <= 1200.00:
    a = x + x * 0.1
    b = x * 0.1
    c = 10

    print('Novo salario: %.2f' % (a))
    print('Reajuste ganho: %.2f' % (b))
    print('Em percentual: %d %%' % (c))

elif 1200.01 <= x <= 2000.00:
    a = x + x * 0.07
    b = x * 0.07
    c = 7

    print('Novo salario: %.2f' % (a))
    print('Reajuste ganho: %.2f' % (b))
    print('Em percentual: %d %%' % (c))

elif x > 2000.00:
    a = x + x * 0.04
    b = x * 0.04
    c = 4

    print('Novo salario: %d' % (a))
    print('Reajuste ganho: %d' % (b))
    print('Em percentual: %d %%' % (c))
