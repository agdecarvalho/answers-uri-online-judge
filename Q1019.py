seconds = int(input())

h = seconds//3600
m = (seconds-h*3600)//60
s = seconds-h*3600-m*60

print('%d:%d:%d' % (h, m, s))
