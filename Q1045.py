a, b, c = reversed(sorted([float(x) for x in input().split()]))

if a >= (b + c):
    print('NAO FORMA TRIANGULO')
else:
    if a**2 == b**2 + c**2:
        print('TRIANGULO RETANGULO')
    elif a**2 > b**2 + c**2:
        print('TRIANGULO OBTUSANGULO')
        if a == b != c or a != b == c or a == c != b:
                print('TRIANGULO ISOSCELES')
    elif a**2 < b**2 + c**2:
        print('TRIANGULO ACUTANGULO')
        if a == b == c:
                print('TRIANGULO EQUILATERO')
        elif a == b != c or a != b == c or a == c != b:
                print('TRIANGULO ISOSCELES')