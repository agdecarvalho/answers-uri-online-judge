a, b = [float(x) for x in input().split()]
c, d = [float(x) for x in input().split()]

dist = ((c-a)**2 + (d-b)**2)**0.5

print('%.4f' % dist)
