a, b, c, d = [int(x) for x in input().split()]

if a < c:
    if b < d:
        print('O JOGO DUROU %d HORA(S) E %d MINUTO(S)' % (c-a, d-b))
    elif b == d:
        print('O JOGO DUROU %d HORA(S) E 0 MINUTO(S)' % (c-a))
    elif b > d:
        print('O JOGO DUROU %d HORA(S) E %d MINUTO(S)' % (c-a-1, (d+60)-b))
elif a == c:
    if b > d:
        print('O JOGO DUROU 23 HORA(S) E %d MINUTO(S)' % ((d+60)-b))
    elif b == d:
        print('O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)')
    elif b < d:
        print('O JOGO DUROU 24 HORA(S) E %d MINUTO(S)' % (d-b))
elif a > c:
    if b < d:
        print('O JOGO DUROU %d HORA(S) E %d MINUTO(S)' % ((24-a)+c, d-b))
    elif b == d:
        print('O JOGO DUROU %d HORA(S) E 0 MINUTO(S)' % ((24-a)+c))
    elif b > d:
        print('O JOGO DUROU %d HORA(S) E %d MINUTO(S)' % ((24-a)+c-1, (d+60)-b))
