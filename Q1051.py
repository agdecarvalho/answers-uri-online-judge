def salary(amount):
    if 0 < amount <= 2000.00:
        return 'Isento'
    elif 2000.01 <= amount <= 3000.00:
        charged = amount - 2000.00
        taxe = charged * 0.08
    elif 3000.01 <= amount <= 4500.00:
        charged = amount - 3000.00
        taxe = charged * 0.18 + 80.00
    elif amount > 4500.00:
        charged = amount - 4500.00
        taxe = charged * 0.28 + 80.00 + 270.00
    return 'R$ %.2f' % (taxe)

# print(salary(float(input())))
